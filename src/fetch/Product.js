const url = `${process.env.REACT_APP_API_URL}/products`

const getAllProducts = () => {
  let access_token = localStorage.getItem('token') 

  return fetch(url + '/all-available', {
    headers: {
      Authorization: 'Bearer ' + access_token
    }
  })
  .then(response => response.json())
  .then(result => {
      return result
    })
}

const getMerchantProducts = () => {
  let access_token = localStorage.getItem('token')
  return fetch(url + '/all-products', {
    method: 'GET',
    headers: {
      Authorization: 'Bearer ' + access_token
    }
  })
    .then(response => response.json())
    .then(result => {
      return result
    })
}

const createOrUpdateProduct = (product) => {
  let access_token = localStorage.getItem('token')
  return fetch(url + product.url, {
    method: product.method,
    headers: {
      "Content-Type": "application/json",
      Authorization: 'Bearer ' + access_token
    },
    body: JSON.stringify({
      category: product.category,
      name: product.name,
      description: product.description,
      price: product.price,
      stocks: product.stocks
    })
  })
    .then(response => response.json())
    .then(result => {
      return result
    })
}

const toggleProductStatus = (productId, productStatus) => {
  let access_token = localStorage.getItem('token')
  return fetch(url + '/toggle/' + productId, {
    method: 'PATCH',
    headers: {
      "Content-Type": "application/json",
      Authorization: 'Bearer ' + access_token
    },
    body: JSON.stringify({
      status: productStatus
    })
  })
    .then(response => response.json())
    .then(result => {
      return result
    })
}

const sortSpecificCategory = (category) => {
  let access_token = localStorage.getItem('token') 

  return fetch(url + '/' + category, {
    headers: {
      Authorization: 'Bearer ' + access_token
    }
  })
  .then(response => response.json())
  .then(result => {
      return result
    })
}

export { getAllProducts, getMerchantProducts, createOrUpdateProduct, toggleProductStatus, sortSpecificCategory }