import {Container, Row, Col, Card, Form, Accordion, Modal, Button, Table} from 'react-bootstrap'
import '../css/UserProfile.css'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faPencil, faCheck, faXmark} from '@fortawesome/free-solid-svg-icons'
import {useEffect, useState} from 'react';
import {userProfile, orderHistory} from "../fetch/User"
import {useParams} from 'react-router-dom'
import { convertTimestamp } from '../common/Global';


export default function UserProfile(){
	const [userProfileData, setUserProfileData] = useState([])
	const [orderHistoryData, setOrderHistoryData] = useState([])

	const {user_id} = useParams()

	const [firstName, setFirstName] = useState('');
  	const [lastName, setLastName] = useState('');
  	const [mobileNumber, setMobileNumber] = useState('');
  	const [email, setEmail] = useState('');

  	const [modalShow, setModalShow] = useState(false);

  	useEffect(() => {
		userProfile(user_id).then((result) => {
			setUserProfileData(result)
		})

		orderHistory().then((result) => {
			setOrderHistoryData(result)
			console.log(orderHistoryData)
		})

		
	}, [])

	return(
		<>
		<Container>
			<Row>
				<Col className="profile-container">
					<Row>
						<Col className="profile-text">
							<span>PROFILE</span>
							<a href="#" onClick={() => setModalShow(true)} className="edit"><FontAwesomeIcon icon={faPencil} className="edit-icon"></FontAwesomeIcon></a>
						</Col>
					</Row>

					<Row>
						<Col>
							<Card className="p-2">
								<Form className="profile">
										<Form.Group>
										    <Form.Label>First Name</Form.Label>
										    <Form.Control 
										        type="text"
										        disabled
										        placeholder={userProfileData.firstName}
										    />
										</Form.Group>

										<Form.Group>
										    <Form.Label>Last Name</Form.Label>
										    <Form.Control 
										        type="text"
										        disabled
										        placeholder={userProfileData.lastName}
										    />
										</Form.Group>

										<Form.Group>
										    <Form.Label>Mobile Number</Form.Label>
										    <Form.Control 
										        type="text"
										        disabled
										        placeholder={userProfileData.mobileNo}
										    />
										</Form.Group>

									    <Form.Group>
									        <Form.Label>Email address</Form.Label>
									        <Form.Control 
									            type="email"
									            disabled
										        placeholder={userProfileData.email}
									        />
									    </Form.Group>
									</Form>
							</Card>
						</Col>
					</Row>
				</Col>

				<Col className="history-container">
					<Row>
						<Col className="history-text">
							<span>ORDER HISTORY</span>
						</Col>
					</Row>

					<Row>
						<Col>
							<Card>
								<Accordion>
								{orderHistoryData.map((order, key) => (
										<Accordion.Item eventKey={key}>
										<Accordion.Header>Purchased On: {convertTimestamp(order.purchasedOn)}</Accordion.Header>
										<Accordion.Body>
											<Table striped bordered hover size="sm" className="text-center" responsive>
												  <thead className="accordion-table">
													  <tr className="istok-font accordion-table-text">
														  <th>PRODUCT ID</th>
														  <th width="20%">NAME</th>
														  <th>PRICE</th>
														  <th>QUANTITY</th>
														  <th>SUBTOTAL</th>
														</tr>
												  </thead>
												  <tbody>
												  {order.products.map((singleOrder, key) => (
														<tr>
															<td width="5%">{singleOrder._id}</td>
															<td>{singleOrder.productName}</td>
															<td>{singleOrder.productPrice}</td>
															<td>{singleOrder.quantity}</td>
															<td>&#8369;{singleOrder.productPrice * singleOrder.quantity}</td>
													  	</tr>
													))}
														<tr>
															<td colSpan={2}></td>
															<td colSpan={2}><strong>TOTAL AMOUNT:</strong></td>
															<td colSpan={4}>&#8369;{order.totalAmount}</td>
														</tr>
														<tr>
															<td colSpan={2}></td>
															<td colSpan={2}><strong>ORDER STATUS:</strong></td>
															<td colSpan={4}>{order.status}</td>
														</tr>
												  </tbody>
											  </Table>
											</Accordion.Body>
									</Accordion.Item>
									))}
								</Accordion>
							</Card>
						</Col>
					</Row>
				</Col>
			</Row>
		</Container>

		<Modal size="lg" centered show={modalShow} onHide={() => setModalShow(false)}>
			    <Modal.Header closeButton className="modal-edit-title">
			        <Modal.Title id="contained-modal-title-vcenter">
			          EDIT PROFILE
			        </Modal.Title>
			    </Modal.Header>
			    <Modal.Body>
			        <Card className="p-2">
			        	<Form className="edit-profile py-3 px-4">
			        		<Form.Group>
			        		    <Form.Label>First Name</Form.Label>
			        		    <Form.Control 
			        		        type="text"
			        		    />
			        		</Form.Group>

			        		<Form.Group>
			        		    <Form.Label>Last Name</Form.Label>
			        		    <Form.Control 
			        		        type="text"
			        		    />
			        		</Form.Group>

			        		<Form.Group>
			        		    <Form.Label>Mobile Number</Form.Label>
			        		    <Form.Control 
			        		        type="number"
			        		    />
			        		</Form.Group>

			        	    <Form.Group>
			        	        <Form.Label>Email address</Form.Label>
			        	        <Form.Control 
			        	            type="email"
			        	        />
			        	    </Form.Group>
			        	</Form>
	        		    <Container className="changes-container">
	        	        	<Row className="py-3 changes-row-container">
	        	        		<Col>
	        	        			<Button variant="outline-secondary" className="save-button">SAVE <FontAwesomeIcon icon={faCheck}></FontAwesomeIcon></Button>
	        	        		</Col>
	        	        		<Col className="px-5">
	        	        		
	        	        		</Col>
	        	        		<Col>
	        	        			<Button variant="outline-secondary" className="cancel-change-button">CANCEL <FontAwesomeIcon icon={faXmark}></FontAwesomeIcon></Button>
	        	        		</Col>
	        	        	</Row>
	        		    </Container>
			        </Card>
			    </Modal.Body>
			</Modal>
		</>
	)
}