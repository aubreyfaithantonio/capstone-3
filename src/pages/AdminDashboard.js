import {Container, Row, Col, Card, Button, Table} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import '../css/AdminDashboard.css'
import {allUsers, allMerchants, allProducts} from "../fetch/Admin";
import {useEffect, useState} from "react";
import {convertTimestamp} from "../common/Global";

export default function AdminDashboard(){
	const [allUsersData, setAllUsersData] = useState([])
	const [allMerchantsData, setAllMerchantsData] = useState([])
	const [allProductsData, setAllProductsData] = useState([])
	
	useEffect(() => {
		allUsers().then((result) => {
			setAllUsersData(result)
		});
		
		allMerchants().then((result) => {
			setAllMerchantsData(result)
		});
		
		allProducts().then((result) => {
			setAllProductsData(result)
		});
		
	}, [])
	
	return(
		<>
		<Container className="admin-tables">
			<Row>
				<Col className="admin-title">
					<span>ADMIN DASHBOARD</span>
				</Col>
			</Row>

			<Row>
				<Col>
					<Row>
						<span><h2>USERS</h2></span>
					</Row>
					<Row>
						<Card className="p-3">
							<Table striped bordered hover size="sm" className="text-center" responsive>
								<thead className="table-title">
									<tr className="istok-font table-title-text">
									    <th>USER ID</th>
									    <th>FIRST NAME</th>
									    <th>LAST NAME</th>
									    <th>EMAIL</th>
									    <th>MOBILE NUMBER</th>
									    <th>JOIN DATE</th>
								  	</tr>
								</thead>
								<tbody>
								{allUsersData.map(user => (
									<tr>
										<td>{user._id}</td>
										<td>{user.firstName}</td>
										<td>{user.lastName}</td>
										<td>{user.email}</td>
										<td>{user.mobileNo}</td>
										<td>{convertTimestamp(user.createdAt)}</td>
									</tr>
								))}
								</tbody>
							</Table>
						</Card>
					</Row>
				</Col>
			</Row>

			<Row>
				<Col>
					<Row>
						<span><h2>MERCHANTS</h2></span>
					</Row>
					<Row>
						<Card className="p-3">
							<Table striped bordered hover size="sm" className="merchants text-center" responsive>
								<thead className="table-title">
									<tr className="istok-font table-title-text">
									    <th>USER ID</th>
									    <th>FIRST NAME</th>
									    <th>LAST NAME</th>
									    <th>EMAIL</th>
									    <th>MOBILE NUMBER</th>
									    <th>JOIN DATE</th>
								  	</tr>
								</thead>
								<tbody>
								{allMerchantsData.map(user => (
									<tr>
										<td>{user._id}</td>
										<td>{user.firstName}</td>
										<td>{user.lastName}</td>
										<td>{user.email}</td>
										<td>{user.mobileNo}</td>
										<td>{convertTimestamp(user.createdAt)}</td>
									</tr>
								))}
								</tbody>
							</Table>
						</Card>
					</Row>
				</Col>
			</Row>

			<Row>
				<Col className="products">
					<Row>
						<span><h2>PRODUCTS</h2></span>
					</Row>

					<Row>
						<Card className="p-3">
							<Table striped bordered hover size="sm" className="text-center" responsive>
								<thead className="table-title">
									<tr className="istok-font table-title-text">
									    <th>PRODUCT ID</th>
									    <th width="20%">NAME </th>
									    <th width="40%">DESCRIPTION</th>
									    <th>PRICE</th>
									    <th>STOCKS</th>
									    <th>CREATE DATE</th>
								  	</tr>
								</thead>
								<tbody>
								{allProductsData.map(product => (
									<tr>
										<td>{product._id}</td>
										<td>{product.name}</td>
										<td>{product.description}</td>
										<td>{product.price}</td>
										<td>{product.stocks}</td>
										<td>{convertTimestamp(product.createdAt)}</td>
									</tr>
								))}
								</tbody>
							</Table>
						</Card>
					</Row>
				</Col>
			</Row>
		</Container>
		</>
	)
}