import {Button, Col, Container, Row, Card, Table, Modal, Tab, Tabs} from "react-bootstrap";
import ProductModal from "../components/ProductModal";
import React, {useEffect, useState} from 'react';
import '../css/MerchantDashboard.css'
import {getMerchantProducts, toggleProductStatus} from "../fetch/Product";
import {basicAlert, convertTimestamp} from "../common/Global";

export default function MerchantDashboard() {
  const [productData, setProductData] = useState({
      name: '',
      category: '',
      description: '',
      quantity: 0,
      price: 0
    });
    const [modalShow, setModalShow] = useState(false);
    const [merchantProducts, setMerchantProducts] = useState([]);
    const [countListener, setCountListener] = useState(0)
    const toggleArchive = (productId, status) => {
      toggleProductStatus(productId, status).then(result => {
        let newCountListener = countListener + 1
        setCountListener(newCountListener)
        basicAlert('success', 'Success!', result.message)
      })
    }
    const setDataToModal = (productData = null) => {
      let modalData = productData === null ?
        {
          id: '',
          category: '',
          name: '',
          description: '',
          stocks: 0,
          price: 0,
          action: 'create',
          modalAction: 'Save',
        } :
        {
          id: productData._id,
          category: productData.category,
          name: productData.name,
          description: productData.description,
          stocks: productData.stocks,
          price: productData.price,
          action: 'update',
          modalAction: 'Save Changes'
        }
      setModalShow(true);
      setProductData(modalData);
    }
    const modalEventListener = () => {
      let newCountListener = countListener + 1
      setCountListener(newCountListener)
    }
    
    useEffect(() => {
      getMerchantProducts().then(result => {
        setMerchantProducts(result)
      })
    }, [countListener])
  
  return (
    <>
      <Container className="mt-5">
        <Row className="merchant-row">
          <Col className="px-4">
            <Row className="checkout-header justify-content-center bebas-font">
              <Col className="checkout-text d-flex justify-content-center">
                <span>MERCHANT DASHBOARD</span>
              </Col>
            </Row>
            <Row className="checkout-details">
              <Col>
                <Row>
                  <Card className="product-tabs">
                  <Col className="table-info px-3 py-2 d-flex justify-content-between bebas-font">
                    <span className="products-text">PRODUCTS</span>
                    <Button onClick={() => setDataToModal()} variant="light" type='button' className="add-item-btn px-5">Create New Item</Button>
                  </Col>
                    <Card.Body>
                      <Table striped bordered hover className="product-table text-center" responsive>
                        <thead className="table-title">
                        <tr className="istok-font table-title-text">
                          <th>PRODUCT ID</th>
                          <th>CATEGORY</th>
                          <th width="10%">NAME</th>
                          <th width="20%">DESCRIPTION</th>
                          <th>PRICE</th>
                          <th>STOCKS</th>
                          <th>DATE CREATED</th>
                          <th>ACTIONS</th>
                        </tr>
                        </thead>
                        <tbody>
                        {merchantProducts.map((product, key) => (
                          <tr key={key}>
                            <td>{product._id}</td>
                            <td>{product.category}</td>
                            <td>{product.name}</td>
                            <td>{product.description}</td>
                            <td>{product.price}</td>
                            <td>{product.stocks}</td>
                            <td>{convertTimestamp(product.createdAt)}</td>
                            <td>
                              <Button onClick={() => setDataToModal(product)} variant="light" type='button' className="edit-btn">Edit</Button>
                              {product.isActive === true ?
                                <Button onClick={() => toggleArchive(product._id, false)} variant="light" type='button' className="archive-product">Archive</Button> :
                                <Button onClick={() => toggleArchive(product._id, true)} variant="light" type='button' className="unarchive-product">Unarchive</Button> }
                            </td>
                          </tr>
                        ))}
                        {merchantProducts.length === 0 ?
                          <tr>
                            <td colSpan={8}>You don't have products yet</td>
                          </tr>
                          :
                          ''
                        }
                        </tbody>
                      </Table>
                    </Card.Body>
                  </Card>
                </Row>
              </Col>
            </Row>
          </Col>
      
          <Col className="all-orders my-4">
            <Card className="p-4 order-tabs">
              <span className="order-text"><h1>ORDERS</h1></span>
              <Tabs defaultActiveKey="confirmed" justify className="tab-container">
                  <Tab eventKey="confirmed" title="Confirmed">
                    <Card>
                      <Table striped bordered className="text-center" responsive>
                        <thead className="table-title">
                        <tr className="istok-font table-title-text">
                          <th>NAME</th>
                          <th width="50%">DESCRIPTION</th>
                          <th>PRICE</th>
                          <th>QUANTITY</th>
                          <th>SUBTOTAL</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                          <td>Pepsi</td>
                          <td>Paloma</td>
                          <td>20</td>
                          <td>2</td>
                          <td>&#8369; 40</td>
                        </tr>
                        <tr>
                          <td>Coke</td>
                          <td>Mismo</td>
                          <td>20</td>
                          <td>1</td>
                          <td>&#8369; 20</td>
                        </tr>
                        <tr>
                          <td>Coke</td>
                          <td>Mismo</td>
                          <td>20</td>
                          <td>1</td>
                          <td>&#8369; 20</td>
                        </tr>
                        <tr>
                          <td>Coke</td>
                          <td>Mismo</td>
                          <td>20</td>
                          <td>1</td>
                          <td>&#8369; 20</td>
                        </tr>
                        <tr>
                          <td>Coke</td>
                          <td>Mismo</td>
                          <td>20</td>
                          <td>1</td>
                          <td>&#8369; 20</td>
                        </tr>
                        <tr>
                          <td>Coke</td>
                          <td>Mismo</td>
                          <td>20</td>
                          <td>1</td>
                          <td>&#8369; 20</td>
                        </tr>
                        <tr>
                          <td>Coke</td>
                          <td>Mismo</td>
                          <td>20</td>
                          <td>1</td>
                          <td>&#8369; 20</td>
                        </tr>
                        <tr>
                          <td>Coke</td>
                          <td>Mismo</td>
                          <td>20</td>
                          <td>1</td>
                          <td>&#8369; 20</td>
                        </tr>
                        <tr>
                          <td>Coke</td>
                          <td>Mismo</td>
                          <td>20</td>
                          <td>1</td>
                          <td>&#8369; 20</td>
                        </tr>
                        
                        </tbody>
                      </Table>
                    </Card>
                  </Tab>
                  <Tab eventKey="completed" title="Completed">
                   <Card>
                     <Table striped bordered className="text-center" responsive>
                       <thead className="table-title">
                       <tr className="istok-font table-title-text">
                         <th>NAME</th>
                         <th width="50%">DESCRIPTION</th>
                         <th>PRICE</th>
                         <th>QUANTITY</th>
                         <th>SUBTOTAL</th>
                       </tr>
                       </thead>
                       <tbody>
                       <tr>
                         <td>Pepsi</td>
                         <td>Paloma</td>
                         <td>20</td>
                         <td>2</td>
                         <td>&#8369; 40</td>
                       </tr>
                       <tr>
                         <td>Coke</td>
                         <td>Mismo</td>
                         <td>20</td>
                         <td>1</td>
                         <td>&#8369; 20</td>
                       </tr>
                       <tr>
                         <td>Coke</td>
                         <td>Mismo</td>
                         <td>20</td>
                         <td>1</td>
                         <td>&#8369; 20</td>
                       </tr>
                       <tr>
                         <td>Coke</td>
                         <td>Mismo</td>
                         <td>20</td>
                         <td>1</td>
                         <td>&#8369; 20</td>
                       </tr>
                       <tr>
                         <td>Coke</td>
                         <td>Mismo</td>
                         <td>20</td>
                         <td>1</td>
                         <td>&#8369; 20</td>
                       </tr>
                       <tr>
                         <td>Coke</td>
                         <td>Mismo</td>
                         <td>20</td>
                         <td>1</td>
                         <td>&#8369; 20</td>
                       </tr>
                       <tr>
                         <td>Coke</td>
                         <td>Mismo</td>
                         <td>20</td>
                         <td>1</td>
                         <td>&#8369; 20</td>
                       </tr>
                       <tr>
                         <td>Coke</td>
                         <td>Mismo</td>
                         <td>20</td>
                         <td>1</td>
                         <td>&#8369; 20</td>
                       </tr>
                       <tr>
                         <td>Coke</td>
                         <td>Mismo</td>
                         <td>20</td>
                         <td>1</td>
                         <td>&#8369; 20</td>
                       </tr>
                       
                       </tbody>
                     </Table>
                   </Card>
                  </Tab>
                  <Tab eventKey="cancelled" title="Cancelled">
                    <Card>
                      <Table striped bordered className="text-center" responsive>
                        <thead className="table-title">
                        <tr className="istok-font table-title-text">
                          <th>NAME</th>
                          <th width="50%">DESCRIPTION</th>
                          <th>PRICE</th>
                          <th>QUANTITY</th>
                          <th>SUBTOTAL</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                          <td>Pepsi</td>
                          <td>Paloma</td>
                          <td>20</td>
                          <td>2</td>
                          <td>&#8369; 40</td>
                        </tr>
                        <tr>
                          <td>Coke</td>
                          <td>Mismo</td>
                          <td>20</td>
                          <td>1</td>
                          <td>&#8369; 20</td>
                        </tr>
                        <tr>
                          <td>Coke</td>
                          <td>Mismo</td>
                          <td>20</td>
                          <td>1</td>
                          <td>&#8369; 20</td>
                        </tr>
                        <tr>
                          <td>Coke</td>
                          <td>Mismo</td>
                          <td>20</td>
                          <td>1</td>
                          <td>&#8369; 20</td>
                        </tr>
                        <tr>
                          <td>Coke</td>
                          <td>Mismo</td>
                          <td>20</td>
                          <td>1</td>
                          <td>&#8369; 20</td>
                        </tr>
                        <tr>
                          <td>Coke</td>
                          <td>Mismo</td>
                          <td>20</td>
                          <td>1</td>
                          <td>&#8369; 20</td>
                        </tr>
                        <tr>
                          <td>Coke</td>
                          <td>Mismo</td>
                          <td>20</td>
                          <td>1</td>
                          <td>&#8369; 20</td>
                        </tr>
                        <tr>
                          <td>Coke</td>
                          <td>Mismo</td>
                          <td>20</td>
                          <td>1</td>
                          <td>&#8369; 20</td>
                        </tr>
                        <tr>
                          <td>Coke</td>
                          <td>Mismo</td>
                          <td>20</td>
                          <td>1</td>
                          <td>&#8369; 20</td>
                        </tr>
                        
                        </tbody>
                      </Table>
                    </Card>
                  </Tab>
              </Tabs>
            </Card>
          </Col>
        </Row>
      </Container>
      <ProductModal show={modalShow} onHide={() => setModalShow(false)} products={productData} modalEvent={() => modalEventListener()}/>
    </>
  )
}