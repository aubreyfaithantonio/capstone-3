import {Container, Row, Col, InputGroup, Button, Offcanvas, Card, Modal, Form} from 'react-bootstrap';
import CartTable from "../components/CartTable";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faCircleArrowLeft, faCartPlus} from '@fortawesome/free-solid-svg-icons'
import {useContext, useEffect, useState} from 'react'
import '../css/Cart.css'
import UserContext from "../UserContext";
import {getAllProducts} from "../fetch/Product"
import {Navigate, useNavigate} from "react-router-dom";
import Swal from 'sweetalert2'
import { addToCart } from '../fetch/Cart';

export default function Cart(){
	const [show, setShow] = useState(false);
	const {user} = useContext(UserContext)
	const handleClose = () => setShow(false);
  	const handleShow = () => setShow(true);
	const navigate = useNavigate()
	const [allProducts, setAllProducts] = useState([])
	const [productId, setProductId] = useState(0);
	const [productName, setProductName] = useState('');
	const [productDesc, setProductDesc] = useState('');
	const [productPrice, setProductPrice] = useState(0);
	const [productStocks, setProductStocks] = useState(0);
	const [count, setCount] = useState(1);

	const addItemToCart = () => {
		if(count <= 0){
			Swal.fire({
				icon: 'error',
				title: 'Uh-Oh..',
				text: "Please add a quantity before adding to cart."
			})
		} else {
			addToCart(productId, count).then((result) => {
				Swal.fire({
					icon: 'success',
					title: 'Success!',
					text: "Item has been added to cart."
				})

				setModalShow(false)
			})
		}
	}

	const [modalShow, setModalShow] = useState(false);

	const modalData = (product) => {
		setProductId(product._id)
		setProductName(product.name)
		setProductDesc(product.description)
		setProductPrice(product.price)
		setProductStocks(product.stocks)

		setCount(1)
		setModalShow(true)
	}

	useEffect(() => {
		getAllProducts().then((result) => {
			setAllProducts(result)
		})

		if (user.id === null) {
			navigate('/login/user')
		}
		
	}, [])

	return(
		<>
			<CartTable/>
			<Button className="off-canvas" onClick={handleShow}>HOT PRODUCTS</Button>

			<Offcanvas show={show} onHide={handleClose} placement={'end'}>
	        	<Offcanvas.Header closeButton>
	          		<Offcanvas.Title>HOT PRODUCTS</Offcanvas.Title>
	        	</Offcanvas.Header>
	        	<Offcanvas.Body className="featured">
		          <Container>
		          			<Row>
		          				<Col>
								  {allProducts.map((product, key) => (
									product.stocks <= 5 ? 
									<Card key={key} style={{ width: '18rem' }} border="dark" className="product-card me-2 mb-2">
										<Card.Body>
											<a href="#" onClick={() => modalData(product)} > 
												<Card.Title className="name limit-text">{product.name}</Card.Title>

												<Card.Text className="description1 limit-text">{product.description}</Card.Text>
												<Card.Text className="description1 limit-text">STOCKS LEFT: {product.stocks}</Card.Text>

											</a>

											<Row className="add2cart-price">
												<Col>
													<Card.Text className="price">&#8369;{product.price}</Card.Text>
												</Col>
												<Col>
													<Button variant="outline-secondary" onClick={() => modalData(product)} className="add2cart-button"><FontAwesomeIcon icon={faCartPlus}></FontAwesomeIcon></Button>
												</Col>
											</Row>
										</Card.Body>
									</Card>
									: null
								))}
		          				</Col>
		          			</Row>
		          		</Container>
		        </Offcanvas.Body>
	      	</Offcanvas>

			  <Modal size="lg" aria-labelledby="contained-modal-title-vcenter" centered show={modalShow}
        onHide={() => setModalShow(false)}>

      	<Modal.Header closeButton className="modal-view-title">
        	<Modal.Title id="contained-modal-title-vcenter">PRODUCT ID: {productId}</Modal.Title>
      	</Modal.Header>
      	<Modal.Body>
      		<h6 className="prod-name mb-3"><strong><u>{productName}</u></strong></h6>
      		
        	<h6>DESCRIPTION:</h6>
        	<p>{productDesc}</p>
        	
        	<h4>PRICE: ₱ {productPrice}</h4>
        	<h6>STOCKS: {productStocks}</h6>
      	</Modal.Body>
      	<Modal.Footer>
        	<Container>
        		<Row>
        			<Col className="wrapper">
        				<InputGroup >
    				        <Button className="plus-btn" onClick={() => setCount(count + 1)} variant="outline-secondary">+</Button>
    				        <Form.Control disabled type="number" value={count} className="quantity"/>
    				        <Button className="minus-btn" 
							onClick={ count <= 25 ? () => {
								if(count <= 1){
									setCount(1)
								} else {
									setCount(count-1)
								}
							} : setCount(25) } 
							variant="outline-secondary">-</Button>
        				</InputGroup>

						<Button variant="outline-secondary" className="add2cart-view-button" onClick={addItemToCart}>ADD TO CART <FontAwesomeIcon icon={faCartPlus}></FontAwesomeIcon></Button>
        			</Col>
        		</Row>
        	</Container>
      	</Modal.Footer>
    	</Modal>
		</>
	)
}