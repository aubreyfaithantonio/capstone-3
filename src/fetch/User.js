const url = `${process.env.REACT_APP_API_URL}/users`

const userLogin = (props) => {
  return fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      email: props.email,
      password: props.password
    })
  })
    .then(response => response.json())
    .then(result => {
      return result
    })
}

const userProfile = (user_id) => {
  let access_token = localStorage.getItem('token')

  return fetch(url + '/' + user_id + '/profile', {
    headers: {
      Authorization: 'Bearer ' + access_token
    }
  })
  .then(response => response.json())
  .then(result => {
      return result
    })
}

const orderHistory = () => {
  let access_token = localStorage.getItem('token') 

  return fetch(url + '/orders', {
    headers: {
      Authorization: 'Bearer ' + access_token
    }
  })
  .then(response => response.json())
  .then(result => {
      return result
    })
}

const registerUserToDb = (data) => {
  return fetch(url + '/register', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      firstName: data.firstName,
      lastName: data.lastName,
      email: data.email,
      password: data.password.password,
      mobileNo: data.mobileNumber,
      isMerchant: data.isMerchant
    })
  })
    .then(response => response.json())
    .then(result => {
      return result
    })
}

export { userLogin, userProfile, orderHistory, registerUserToDb }