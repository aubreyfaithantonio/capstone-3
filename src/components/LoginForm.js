import {Form, Button, InputGroup} from 'react-bootstrap'
import css from '../css/LoginForm.css'
import {useState, useEffect, useContext} from 'react'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faEye,faEyeSlash} from '@fortawesome/free-solid-svg-icons'
import Swal from 'sweetalert2'
import {useNavigate} from "react-router-dom";
import { userLogin } from '../fetch/User'
import {basicAlert, validateEmail} from "../common/Global";
import UserContext from "../UserContext";
import UserProfile from "./UserProfile";

export default function LoginForm(props) {

  const opensweetalert = (icon, title, text) => {
      return Swal.fire({
        icon: icon,
        title: title,
        text: text
      })
    }
  const {setUser, unsetUser} = useContext(UserContext)
  const handleClickShowPassword = () => {
      setPassword({ ...password, showPassword: !password.showPassword });
    };
  const handlePasswordChange = (prop) => (event) => {
      setPassword({ ...password, [prop]: event.target.value });
    };
  const handleSubmit = (event) => {
      event.preventDefault()
      let params = {
        email: email,
        password: password.password
      }
      let login_request = userLogin(params)
      login_request.then((result) => {
          if (result.success === false) {
            basicAlert('error', 'Error!', result.message)
          } else if(props.user_level !== result.level){
            unsetUser()
            setUser({
              id: null,
              level: null
            })
            
            UserProfile.setId(null)
            UserProfile.setLevel(null)
            basicAlert('error', 'Error!', 'You are not allowed to login in as ' + props.user_level).then(() => {
              navigate('/login/' + result.level)
            })
          } else {
            UserProfile.setToken(result.access)
            setUser({
              id: result.id,
              level: result.level
            })
            UserProfile.setId(result.id)
            UserProfile.setLevel(result.level)
            basicAlert('success', 'Success!', 'You are now logged in!').then(() => {
              if (result.level === 'merchant') {
                navigate(props.redirect_link + result.id)
              } else {
                navigate(props.redirect_link)
              }
            })
          }
      })
    }

  const navigate = useNavigate()

  const [email, setEmail] = useState('')
  const [isDisabled, setIsDisabled] = useState(true)

  const [password, setPassword] = useState({
    password: "",
    showPassword: false,
  });
  
  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };
  
  useEffect(() => {
     setIsDisabled(true)
     if(email !== '' && password.password !== '' && validateEmail(email)){
       setIsDisabled(false)
     }
   }, [email, password])
  
  return (
    <>
      <Form className="font-24" onSubmit={handleSubmit}>
        <Form.Group controlId="userEmail">
          <Form.Label className="bebas-font">EMAIL:</Form.Label>
          <Form.Control placeholder="username@email.com" type="email" required value={email} onChange={event => setEmail(event.target.value)} />
        </Form.Group>
        <br/>
        <Form.Group controlId="password">
            <Form.Label className="bebas-font">PASSWORD:</Form.Label>
            <InputGroup>
                <Form.Control type={password.showPassword ? "text" : "password"}  required  placeholder="Password" onChange={handlePasswordChange("password")}  value={password.password} />
                <InputGroup.Text className="see-icon" onClick={handleClickShowPassword} id="basic-addon2">{password.showPassword ? <FontAwesomeIcon icon={faEyeSlash}></FontAwesomeIcon> : <FontAwesomeIcon onClick={handleClickShowPassword}icon={faEye} className="see-icon"></FontAwesomeIcon>}
                </InputGroup.Text>
            </InputGroup>
        </Form.Group>
        <br/>
        <div className="d-flex justify-content-center bebas-font">
          <Button onClick={handleSubmit} disabled={isDisabled} variant="primary" type='submit' id="sign-in-btn" className="px-5 py-3">SIGN IN</Button>
        </div>
      </Form>
    </>
  )
}