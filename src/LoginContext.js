import React from 'react'

// Initializes a react context
const LoginContext = React.createContext()

// Initializes a context provider
// Gives us ability to provide a specific context through a component
export const LoginProvider = LoginContext.Provider

export default LoginContext