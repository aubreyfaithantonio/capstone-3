import {Navbar, Nav, Container, Row, Col, Card, Button, Form, Modal, InputGroup} from 'react-bootstrap'
import '../css/ShopPage.css'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faMagnifyingGlass, faCartPlus, faPesoSign} from '@fortawesome/free-solid-svg-icons'
import {useState, useEffect, useContext} from 'react';
import {getAllProducts, sortSpecificCategory} from "../fetch/Product"
import {useNavigate} from "react-router-dom";
import UserContext from "../UserContext";
import Swal from 'sweetalert2'
import { addToCart } from '../fetch/Cart';

export default function ShopPage(){

	const {user} = useContext(UserContext)

	const navigate = useNavigate()
	const [allProducts, setAllProducts] = useState([]);
	const [productId, setProductId] = useState(0);
	const [productName, setProductName] = useState('');
	const [productDesc, setProductDesc] = useState('');
	const [productPrice, setProductPrice] = useState(0);
	const [productStocks, setProductStocks] = useState(0);
	const [count, setCount] = useState(1)
	

	const addItemToCart = () => {
		if(count <= 0){
			Swal.fire({
				icon: 'error',
				title: 'Uh-Oh..',
				text: "Please add a quantity before adding to cart."
			})
		} else {
			addToCart(productId, count).then((result) => {
				Swal.fire({
					icon: 'success',
					title: 'Success!',
					text: "Item has been added to cart."
				})

				setModalShow(false)
			})
		}
	}

	const categorySorter = (category) => {
		sortSpecificCategory(category).then((result) => {
			setAllProducts(result)
		})
	}

	const sortAll = () => {
		getAllProducts().then((result) => {
			setAllProducts(result)
		})
	}

	const cannotAdd = () => {
		if(user.id === null){
			navigate('/login/user')
		} else {
			addItemToCart()
		}
	}
	useEffect(() => {
		// getAllProducts().then((result) => {
		// 	if('auth' in result){
		// 		localStorage.clear()
		// 		Swal.fire({
		// 		  icon: 'error',
		// 		  title: 'Oops...',
		// 		  text: result.auth
		// 		}).then(() => {
		// 			navigate('/')
		// 		})
		// 	} else {
		// 		setAllProducts(result)
		// 	}
			
		// })

		getAllProducts().then((result) => {
			setAllProducts(result)
		})
		
	}, [])

	const [modalShow, setModalShow] = useState(false);

	const modalData = (product) => {
		setProductId(product._id)
		setProductName(product.name)
		setProductDesc(product.description)
		setProductPrice(product.price)
		setProductStocks(product.stocks)

		setCount(1)
		setModalShow(true)
	}
	return(
		<>
		<Container fluid className="mt-5">
			<Row>
				<Col className="sidenav col-4">
					<Nav className="sidenav-categories px-3 py-5 justify-content-center align-item-center">
						<Row className="flex-column ">
							<Row className="flex-column">
								<Col><h3 className="categories-text">CATEGORIES</h3></Col>    
							</Row>
							<Row className="categories flex-column">
								<Col><Nav.Item >
								<Form className="d-flex">
						    	  <Form.Control
						    	    type="search"
						    	    placeholder="Search"
						    	    className="me-2 search-input"
						    	    aria-label="Search"
						    	  />
						    	  <Button variant="outline-secondary" className="search-button"><FontAwesomeIcon icon={faMagnifyingGlass} className="search-icon"></FontAwesomeIcon></Button>
						    	</Form>
								</Nav.Item></Col>
								<br/>
								<Col><Nav.Link className="sidenav-link" eventKey="link-1" onClick={() => sortAll()}>ALL PRODUCTS</Nav.Link></Col>
								<Col><Nav.Link className="sidenav-link" eventKey="link-2" onClick={() => categorySorter('Drinks & Beverages')}>DRINKS & BEVERAGES</Nav.Link></Col>	
								<Col><Nav.Link className="sidenav-link" eventKey="link-3" onClick={() => categorySorter('Desserts & Snacks')}>DESSERTS & SNACKS</Nav.Link></Col>	
								<Col><Nav.Link className="sidenav-link" eventKey="link-4" onClick={() => categorySorter('Cooking Essentials')}>COOKING ESSENTIALS</Nav.Link></Col>	
								<Col><Nav.Link className="sidenav-link" eventKey="link-5" onClick={() => categorySorter('Health & Beauty')}>HEALTH & BEAUTY</Nav.Link></Col>	
								<Col><Nav.Link className="sidenav-link" eventKey="link-6" onClick={() => categorySorter('Household Supplies')}>HOUSEHOLD SUPPLIES</Nav.Link></Col>	
								<Col><Nav.Link className="sidenav-link" eventKey="link-6" onClick={() => categorySorter('Cellphone Load')}>CELLPHONE LOAD</Nav.Link></Col>	
								<Col><Nav.Link className="sidenav-link" eventKey="link-7" onClick={() => categorySorter('Miscellaneous')}>MISCELLANEOUS</Nav.Link></Col>
							</Row>
						</Row>
					</Nav>
				</Col>

				<Col className="products-shop col-8">
					<Row>
						{allProducts.map((product, key) => (
							<Card key={key} style={{ width: '18rem' }} border="dark" className="product-card me-2 mb-2">
					      	    <Card.Body>
					      	      	<a href="#" onClick={() => modalData(product)} > 
					      	      		<Card.Title className="name limit-text">{product.name}</Card.Title>

					      	      		<Card.Text className="description1 limit-text">{product.description}</Card.Text>
					      	      	</a>

					      	      	<Row className="add2cart-price">
					      	      	  	<Col>
					      	      	  		<Card.Text className="price">&#8369;{product.price}</Card.Text>
					      	      	  	</Col>
					      	      	  	<Col>
					            	  		<Button variant="outline-secondary" onClick={() => modalData(product)} className="add2cart-button"><FontAwesomeIcon icon={faCartPlus}></FontAwesomeIcon></Button>
					      	      	  	</Col>
					      	      	</Row>
					      	    </Card.Body>
							</Card>
						))}
					</Row>
				</Col>
			</Row>
		</Container>

		<Modal size="lg" aria-labelledby="contained-modal-title-vcenter" centered show={modalShow}
        onHide={() => setModalShow(false)}>

      	<Modal.Header closeButton className="modal-view-title">
        	<Modal.Title id="contained-modal-title-vcenter">PRODUCT ID: {productId}</Modal.Title>
      	</Modal.Header>
      	<Modal.Body>
      		<h6 className="prod-name mb-3"><strong><u>{productName}</u></strong></h6>
      		
        	<h6>DESCRIPTION:</h6>
        	<p>{productDesc}</p>
        	
        	<h4>PRICE: ₱ {productPrice}</h4>
        	<h6>STOCKS: {productStocks}</h6>
      	</Modal.Body>
      	<Modal.Footer>
        	<Container>
        		<Row>
        			<Col className="wrapper">
        				<InputGroup >
    				        <Button className="plus-btn" onClick={() => setCount(count + 1)} variant="outline-secondary">+</Button>
    				        <Form.Control disabled type="number" value={count} className="quantity"/>
    				        <Button className="minus-btn" 
							onClick={ count <= 25 ? () => {
								if(count <= 1){
									setCount(1)
								} else {
									setCount(count-1)
								}
							} : setCount(25) } 
							variant="outline-secondary">-</Button>
        				</InputGroup>

						<Button variant="outline-secondary" className="add2cart-view-button" onClick={() => cannotAdd()}>ADD TO CART <FontAwesomeIcon icon={faCartPlus}></FontAwesomeIcon></Button>
        			</Col>
        		</Row>
        	</Container>
      	</Modal.Footer>
    	</Modal>
		</>

	)
}