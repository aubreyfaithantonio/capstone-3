import {Table} from "react-bootstrap";
import css from '../css/Checkout.css'
import {cartDetails} from "../fetch/Cart";
import {useEffect, useState} from "react";

export default function OrdersTable() {
  const [orderData, setOrderData] = useState({
		userId:'',
		products: [],
		subtotalAmount: [0]
	});

  useEffect(() => {
		cartDetails().then(result => {
			setOrderData(result)
		})
	},[])

 

  return (
    <>
      <Table striped bordered className="orders text-center" responsive>
        <thead className="table-title">
        <tr className="istok-font table-title-text">
          <th>NAME</th>
          <th width="50%">DESCRIPTION</th>
          <th>PRICE</th>
          <th>QUANTITY</th>
          <th>SUBTOTAL</th>
        </tr>
        </thead>
        <tbody>
        {orderData.products.map((order , key) => (
            <tr key={key}>
              <td>{order.productName}</td>
              <td>{order.productDescription}</td>
              <td>&#8369;{order.productPrice}</td>
              <td>{order.quantity}</td>
              <td>{orderData["subtotalAmount"][key]}</td>
            </tr>
          ))}
          {(orderData.products.length === 0) ?
            <tr>
              <td colSpan={7}>Your cart is empty</td>
            </tr>
            :
            ''
					}
        </tbody>
      </Table>
    </>
  );
}