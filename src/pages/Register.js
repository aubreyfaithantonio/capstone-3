import {useEffect, useState} from "react";
import { Container, Row, Card, Col } from 'react-bootstrap';
import Image from 'react-bootstrap/Image'
import RegisterForm from "../components/RegisterForm";
import {useNavigate, useParams} from 'react-router-dom'
import {allowedUserLevels, basicAlert} from "../common/Global";

export default function Register() {
  const {user_level} = useParams()
  const navigate = useNavigate()
  const [currentLogin, setCurrentLogin] = useState('')
  let header_text = currentLogin === 'user' ? 'CREATE AN ACCOUNT' : 'CREATE A MERCHANT ACCOUNT'
  let account_switch_text = currentLogin === 'user' ? 'Login as User' : 'Login as Merchant'
  
  useEffect(() => {
    if (allowedUserLevels.includes(user_level)) {
      setCurrentLogin(user_level)
    } else {
      basicAlert('error', 'Uh oh...', 'Use the proper login page').then(() => {
        navigate('/login/user')
      })
    }
  }, [user_level])
  
  function renderCreateUserImage() {
    if (currentLogin === 'user') {
      return <Col className="col-12 no-padding">
        <div className="user-login-img">
          <Image src="/images/register-user.jpg" fluid></Image>
        </div>
      </Col>
    }
    return ''
  }
  
  function renderCreateMerchantImage() {
    if (currentLogin === 'merchant') {
      return <Col className="col-12 no-padding">
        <div className="merchant-login-img">
          <Image src="/images/register-merchant.jpg" fluid></Image>
        </div>
      </Col>
    }
    return ''
  }
  
  return (
    <>
      <div className="register-card p-5">
        <Container>
          <Row>
            {renderCreateMerchantImage()}
          </Row>
          <Row className="form-field">
            <Row>
              <div className="register-header d-flex justify-content-center bebas-font">
                {header_text}
              </div>
            </Row>
            <RegisterForm user_level={user_level}/>
          </Row>
          <Row>
            {renderCreateUserImage()}
          </Row>
        </Container>
      </div>
    </>
  )
}