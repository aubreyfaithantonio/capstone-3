import {useContext, useState, useEffect} from "react";
import {Container, Row, Col, Button, Card} from 'react-bootstrap';
import { Link } from 'react-router-dom'
import '../css/Checkout.css'
import OrdersTable from "../components/OrdersTable";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faCircleArrowLeft} from '@fortawesome/free-solid-svg-icons'
import {cartDetails} from "../fetch/Cart";
import {checkout, allOrders} from "../fetch/Order";
import { basicAlert } from "../common/Global";
import {useNavigate} from "react-router-dom";


export default function Checkout() {

  const [allOrdersData, setAllOrdersData] = useState([])
  const [orderData, setOrderData] = useState({
		userId:'',
		products: [],
		subtotalAmount: [0]
	});

  const totalWithSf = () => {
    let sum = parseInt(orderData.subtotalAmount) + 50
    return sum
  }

  const overallTotal = () => {
    let sum = (parseInt(orderData.subtotalAmount) + 50) - 20
    return sum
  }
  const navigate = useNavigate()

  const checkoutOrder = () => {
    checkout(orderData._id).then((result) => {
			basicAlert('success', 'Success!', result.message).then(() => {
        navigate('/shop')
			})
		})
  }

  useEffect(() => {             
		cartDetails().then(result => {
			setOrderData(result)
		})

    // allOrders().then(result => {
		// 	setAllOrdersData(result)
    //   console.log(allOrdersData)
		// })
	},[checkoutOrder])

  return (
    <>
      <Container className="mt-5">
        <Row>
          <Col className="col-9 px-4">
            <Row className="checkout-header justify-content-center bebas-font">
              <Card className="checkout-table">
                <Col className="checkout-text">
                  <Button variant="outline-light" as={Link} to="/cart" className="back"><FontAwesomeIcon icon={faCircleArrowLeft} className="back-icon"></FontAwesomeIcon></Button>
                  <span>CHECKOUT</span>
                </Col>
              </Card>
            </Row>
            <Row className="checkout-details">
              <Card className="p-3 checkout-table">
                <OrdersTable />
              </Card>
            </Row>
          </Col>

          <Col className="col-3 p-5 checkout-details">
            <Row className="delivery-address-details mb-5 px-3">
              <div className="delivery-address bebas-font text-center">
                Delivery Address:
              </div>
              <textarea name="del_address" id="" cols="1" rows="3" disabled></textarea>
            </Row>
            <Row className="istok-font">
              <Col className="d-flex justify-content-between">
                <div>ITEM SUBTOTAL</div>
                <div>&#8369; {orderData["subtotalAmount"]}</div>
              </Col>
            </Row>
            <Row className="istok-font">
              <Col className="d-flex justify-content-between">
                <div>DELIVERY FEE</div>
                <div>&#8369; 50</div>
              </Col>
            </Row>
            <Row className="istok-font">
              <Col className="d-flex justify-content-between">
                <div><strong>TOTAL WITH DF:</strong></div>
                <div><strong>&#8369; {totalWithSf()}</strong></div>
              </Col>
            </Row>
            <br/>
            <Row className="istok-font">
              <Col className="d-flex justify-content-between">
                <div>COUPONS</div>
                <div>- &#8369; 20</div>
              </Col>
            </Row>
            <Row className="total-amount bebas-font">
              <Col className="d-flex justify-content-between total-container">
                <div>TOTAL</div>
                <div>&#8369; {overallTotal()}</div>
              </Col>
            </Row>
            <Row>
              <Button className="confirm-btn" onClick={() => checkoutOrder()}>CONFIRM ORDER</Button>
            </Row>
          </Col>
        </Row>
      </Container>
    </>
  )
}