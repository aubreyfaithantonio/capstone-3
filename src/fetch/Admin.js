const url = `${process.env.REACT_APP_API_URL}/admin`

const allUsers = () => {
  let access_token = localStorage.getItem('token')
  return fetch(url + '/allUsers', {
    method: 'GET',
    headers: {
      Authorization: 'Bearer ' + access_token
    }
  })
    .then(response => response.json())
    .then(result => {
      return result
    })
}

const allMerchants = () => {
  let access_token = localStorage.getItem('token')
  return fetch(url + '/allMerchants', {
    headers: {
      Authorization: 'Bearer ' + access_token
    }
  })
    .then(response => response.json())
    .then(result => {
      return result
    })
}

const allProducts = () => {
  let access_token = localStorage.getItem('token')
  return fetch(url + '/allProducts', {
    headers: {
      Authorization: 'Bearer ' + access_token
    }
  })
    .then(response => response.json())
    .then(result => {
      return result
    })
}

export { allUsers, allMerchants, allProducts }