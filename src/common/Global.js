import Swal from "sweetalert2";

const validateEmail = (email_address) => {
  let validEmailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return !!String(email_address).toLowerCase().match(validEmailRegex)
}

const convertTimestamp = (timestamp) => {
  const options = { year: "numeric", month: "numeric", day: "numeric"}
  return new Date(timestamp).toLocaleDateString(undefined, options)
}

const basicAlert = (icon, title, text) => {
  return Swal.fire({
    icon: icon,
    title: title,
    text: text
  })
}

const allowedUserLevels = ['user', 'merchant', 'admin']

export { validateEmail, convertTimestamp, basicAlert, allowedUserLevels}