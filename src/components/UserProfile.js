let UserProfile = (() => {
    let getId = () => {
      return localStorage.getItem('id')
    }
    
    let getLevel = () => {
      return localStorage.getItem('level')
    }
    
    let setId = (id) => {
      localStorage.setItem('id', id)
    }
    
    let setLevel = (level) => {
      localStorage.setItem('level', level)
    }
    
    let getToken = () => {
      localStorage.getItem('token')
    }
    
    let setToken = (token) => {
      localStorage.setItem('token', token)
    }
    
    return {
      getId: getId,
      getLevel: getLevel,
      setId: setId,
      setLevel: setLevel,
      getToken: getToken,
      setToken: setToken
    }
  })();
  
  export default UserProfile;