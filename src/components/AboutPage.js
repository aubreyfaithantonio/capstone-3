import {Container, Row, Col, Card} from 'react-bootstrap'
import '../css/AboutPage.css'

export default function AboutPage(){
	const myStyle={
		height: '87.9vh',
        backgroundImage: "url('/images/aboutBg.jpg')",
        backgroundPosition: 'center',
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        display: 'flex',
        justifyContent: 'center'
    };

    return(
    	<>
    	<div style={myStyle} className="aboutBg">
    		<Container className="about-content">
    			<Row className="about-row">
    				<Col className="col-6">
    					<Card className="local-bg">
				            <Card.Img variant="top" src="/images/local-2.png" />
    					</Card>
    				</Col>

    				<Col className="col-6">
    					<Card className="about-us-bg">
    						<Card.Body className="p-3">
					            <Card.Title className="about-us-title"> ABOUT US </Card.Title>
					            <Card.Text className="about-us-text">
						            <p>
						            	We are a Philippine-based company whose primary goal is to help small business owners, particularly <em>sari sari store owners</em>, to expand their business online.
						            </p>
						            <p>
						            	We believe that <strong><u>local business matters</u></strong> and we want to keep the "filipino sari-sari store" concept alive by making it digital.
						            </p>
					            </Card.Text>
    						</Card.Body>
    					</Card>
    				</Col>
    			</Row>
    		</Container>
    	</div>
    	<div className="overlay-about"></div>
    	</>
    )
}
