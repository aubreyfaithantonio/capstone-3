import { Navigate } from 'react-router-dom'
import {useContext, useEffect, useState} from 'react'
import UserContext from "../UserContext";


export default function Logout(){
  const {user, setUser, unsetUser} = useContext(UserContext)
  const [previousLogin, setPreviousLogin] = useState('/login/user')
  
  // An effect which removes the user email from the global user state that comes from the context
  useEffect(() => {
    setPreviousLogin('/login/' + user)
    setUser({
      id: null,
      level: null
    })
  }, [])
  
  // Using the context, clear the contents of the local storage
  unsetUser()
  
  return(
    <Navigate to={previousLogin}/>
  )
}