import {Navbar, Nav, Container, Row, Col, Button, Form, Modal, InputGroup, Card} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faFacebook, faInstagram, faFacebookMessenger} from '@fortawesome/free-brands-svg-icons'
import {faEnvelope, faPaperPlane, faXmark} from '@fortawesome/free-solid-svg-icons'
import '../css/Footer.css'
import {useEffect, useState} from 'react';
import Swal from 'sweetalert2'

export default function Footer(){

	const [modalShow, setModalShow] = useState(false);
	const [toastShow, setToastShow] = useState(false);

	const opensweetalert = (icon, title, text) => {
      return Swal.fire({
        icon: icon,
        title: title,
        text: text
      })
    }

	const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 4000,
			timerProgressBar: true
		  })
	

	const showToast = () => {
		setModalShow(false)
		Toast.fire({
			icon: 'success',
			title: 'Thank you for your message! Please give us 2-3 days to respond.'
		  })
	}

	return(
		<>
      	<Container >
	      	<Navbar fixed="bottom" className="d-flex justify-content-around footer-container">
		      	<Row>
		      		<Col><Nav.Link className="footer-icon" href="#"><FontAwesomeIcon icon={faFacebook} className="facebook-icon"></FontAwesomeIcon></Nav.Link></Col>
		      		<Col><Nav.Link className="footer-icon" href="#"><FontAwesomeIcon icon={faFacebookMessenger} className="fm-icon"></FontAwesomeIcon></Nav.Link></Col>
		      		<Col><Nav.Link className="footer-icon" href="#"><FontAwesomeIcon icon={faInstagram} className="insta-icon"></FontAwesomeIcon></Nav.Link></Col>
		      		<Col><Nav.Link className="footer-icon" href="#"><FontAwesomeIcon icon={faEnvelope} className="email-icon"></FontAwesomeIcon></Nav.Link></Col>
		      	</Row>

		      	<Row>
		      		<Col><Nav.Item className="footer-copyright" href="#link">COPYRIGHT ⓒ 2022 SARI - SARI. ALL RIGHTS RESERVED.</Nav.Item></Col>
		      	</Row>

		      	<Row>
				  	<Col><Nav.Link className="footer-link" as={Link} to="/login/admin">ADMIN</Nav.Link></Col>
		      		<Col><Nav.Link className="footer-link" href="#" onClick={() => setModalShow(true)}>CONTACT</Nav.Link></Col>
		      	</Row>
		   	</Navbar>
      	</Container>

      	<Modal size="lg" aria-labelledby="contained-modal-title-vcenter" centered show={modalShow}
        onHide={() => setModalShow(false)}>
	      	<Modal.Header closeButton className="modal-view-title">
	        	<Modal.Title id="contained-modal-title-vcenter">
	         	 HOW CAN WE HELP YOU?
	        	</Modal.Title>
	      	</Modal.Header>
		    <Modal.Body>
		        <Card className="p-2">
		        	<Form className="edit-profile py-3 px-4">
		        		<Form.Group>
		        		    <Form.Label>Name</Form.Label>
		        		    <Form.Control 
		        		        type="text"
		        		        placeholder="Your full name"
		        		    />
		        		</Form.Group>

		        		<Form.Group>
		        		    <Form.Label>Email</Form.Label>
		        		    <Form.Control 
		        		        type="email"
		        		        placeholder="username@email.com"
		        		    />
		        		</Form.Group>

		        		<Form.Group>
		        		    <Form.Label>Message:</Form.Label>
		        		    <Form.Control as="textarea" rows={5} placeholder="Type your message here" />
		        		</Form.Group>
		        	</Form>
	    		    <Container className="changes-container">
	    	        	<Row className="py-3 changes-row-container">
	    	        		<Col>
	    	        			<Button variant="outline-secondary" className="save-button" onClick={() => showToast()}>SEND <FontAwesomeIcon icon={faPaperPlane}></FontAwesomeIcon></Button>
	    	        		</Col>
	    	        		<Col className="px-5">
	    	        		
	    	        		</Col>
	    	        		<Col>
	    	        			<Button variant="outline-secondary" className="cancel-change-button" onClick={() => setModalShow(false)}>CANCEL <FontAwesomeIcon icon={faXmark}></FontAwesomeIcon></Button>
	    	        		</Col>
	    	        	</Row>
	    		    </Container>
		        </Card>
		    </Modal.Body>
    	</Modal>
		</>
	)
}