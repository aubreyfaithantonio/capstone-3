import './App.css';
import AppNavbar from './components/AppNavbar'
import {UserProvider} from './UserContext'
import {useState} from 'react'
import Home from './pages/Home'
import Shop from './pages/Shop'
import MerchantPage from './pages/MerchantPage'
import About from './pages/About'
import UserProfile from './pages/UserProfile'
import AdminDashboard from './pages/AdminDashboard'
import Login from './pages/Login'
import Logout from "./components/Logout";
import Register from './pages/Register'
import Checkout from './pages/Checkout'
import Cart from './pages/Cart'
import MerchantDashboard from './pages/MerchantDashboard'
import ErrorPage from './pages/ErrorPage'
import {BrowserRouter as Router, Navigate, Route, Routes} from 'react-router-dom'

function App() {
	const [user, setUser] = useState({
		id: localStorage.getItem('id') !== null ? localStorage.getItem('id') : null,
		level: localStorage.getItem('level') !== null ? localStorage.getItem('level') : null
	})
	const unsetUser = () => {
		localStorage.clear()
	}

	return(
		<>
			<UserProvider value={{user, setUser, unsetUser}}>
				<Router>
					<AppNavbar/>
					<div className="main-container">
						<Routes>
							<Route path="/" element={<Home/>}/>
							<Route path="/about" element={<About/>}/>
							<Route path="/merchant" element={<MerchantPage/>}/>
							<Route path="/shop" element={<Shop/>}/>
							<Route path="/login/:user_level" element={<Login/>}/>
							<Route path="/logout" element={<Logout/>}/>
							<Route path="/register/:user_level" element={<Register/>}/>
							<Route path="/profile/:user_id" element={<UserProfile/>}/>
							<Route path="/admin" element={<AdminDashboard/>}/>
							<Route path="/checkout/:cartId" element={<Checkout/>}/>
							<Route path="/cart" element={<Cart/>}/>
							<Route path="/merchant/:merchant_id" element={<MerchantDashboard/>}/>
							<Route path="/admin" element={<AdminDashboard/>}/>
							<Route path="/login" element={<Navigate to="/login/user" />} />
							<Route path='*' element={<ErrorPage/>}/>
						</Routes>
					</div>
				</Router>
			</UserProvider>
		</>
	)
}

export default App;
