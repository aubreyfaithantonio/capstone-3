import {Container, Button, Row, Col} from 'react-bootstrap'
import '../css/Landing.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faChevronDown} from '@fortawesome/free-solid-svg-icons'
import { Link, NavLink } from 'react-router-dom'

export default function Landing(){
	const myStyle={
		height:'87.9vh',
        backgroundImage: "url('/images/landingBg.jpg')",
        backgroundPosition: 'center',
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        display: 'flex',
        justifyContent: 'center'
    };


	return(
		<>
		<div style={myStyle} className="landingBg">
			<Container className="banner">
				<Row>
					<Col>
						<h1 className="banner-text"><strong>SHOP EVERYDAY NEEDS FROM LOCAL MERCHANTS NEAR YOU</strong></h1>
					</Col>
				</Row>
				<Row>
					<Button as={Link} to="/shop" variant="link"><FontAwesomeIcon icon={faChevronDown} className="down-icon"></FontAwesomeIcon></Button>
				</Row>
			</Container>
		</div>
		<div className="overlay"></div>
		</>
	)
}