import {Form, Button, Col, Row, InputGroup} from 'react-bootstrap'
import {useState, useEffect} from 'react'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faEye,faEyeSlash} from '@fortawesome/free-solid-svg-icons'
import '../css/RegisterForm.css'
import {basicAlert, validateEmail} from "../common/Global";
import {registerUserToDb} from "../fetch/User";
import {useNavigate} from "react-router-dom";

export default function RegisterForm(user_level) {
  const [firstName, setFirstName] = useState('')
  const [lastName, setLastName] = useState('')
  const [mobileNumber, setMobileNumber] = useState('')
  const [email, setEmail] = useState('')
  const [isDisabled, setIsDisabled] = useState(true)
  
  const [password, setPassword] = useState({
    password: "",
    showPassword: false,
  });
  
  const handleClickShowPassword = () => {
    setPassword({ ...password, showPassword: !password.showPassword });
  };
  
  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };
  
  const handlePasswordChange = (prop) => (event) => {
    setPassword({ ...password, [prop]: event.target.value });
  };
  
  const navigate = useNavigate()
  
  function registerUser(event) {
    event.preventDefault()
    
    let data = {
      firstName: firstName,
      lastName: lastName,
      email: email,
      password: password,
      mobileNumber: parseInt(mobileNumber),
      isMerchant: (user_level.user_level === 'merchant')
    }
  
    registerUserToDb(data).then((result) => {
      if (result.success === false) {
        basicAlert('error', 'Uh oh...', result.message)
        return false
      } else {
        basicAlert('success', 'Success', result.message).then(() => {
          navigate('/login/' + user_level.user_level)
        })
      }
    })
  }
  
  useEffect(() => {
    setIsDisabled(true)
    if(email !== '' && password.password !== '' && firstName !== '' && lastName !== '' && mobileNumber.length === 11 && validateEmail(email)){
      setIsDisabled(false)
    }
  }, [firstName, lastName, mobileNumber, email, password])
  
  return (
    <>
      <Form className="font-24 register-form" onSubmit={event => registerUser(event)} method="POST" action={'/user/register'}>
        <Row>
          <Col className="col-6">
            <div className="form-spacer">
              <Form.Group controlId="firstname">
                <Form.Label className="bebas-font">FIRST NAME:</Form.Label>
                <Form.Control
                  minLength="2"
                  type="text"
                  placeholder="Your first name"
                  required
                  value={firstName}
                  onChange={event => setFirstName(event.target.value)}
                />
              </Form.Group>
              <br/>
              <Form.Group controlId="lastname">
                <Form.Label className="bebas-font">LAST NAME:</Form.Label>
                <Form.Control
                  minLength="2"
                  type="text"
                  placeholder="Your last name"
                  required
                  value={lastName}
                  onChange={event => setLastName(event.target.value)}
                />
              </Form.Group>
              <br/>
              <Form.Group controlId="mobilenumber">
                <Form.Label className="bebas-font">MOBILE NUMBER:</Form.Label>
                <Form.Control
                  maxLength="11"
                  type="number"
                  placeholder="09XX XXX XXXX"
                  required
                  value={mobileNumber}
                  onChange={event => setMobileNumber(event.target.value)}
                />
              </Form.Group>
            </div>
          </Col>
          <Col className="col-6">
            <div className="form-spacer">
              <Form.Group controlId="emailaddress">
                <Form.Label className="bebas-font">EMAIL:</Form.Label>
                <Form.Control
                  type="email"
                  placeholder="username@email.com"
                  required
                  value={email}
                  onChange={event => setEmail(event.target.value)}
                />
              </Form.Group>

              <br/>

              <Form.Group controlId="password">
                <Form.Label className="bebas-font">PASSWORD:</Form.Label>
              <InputGroup>
                <Form.Control
                  type={password.showPassword ? "text" : "password"}
                  placeholder="Password"
                  minLength="4"
                  required
                  onChange={handlePasswordChange("password")}
                  value={password.password}
                />

                <InputGroup.Text className="see-icon" id="basic-addon2" onClick={handleClickShowPassword}>{password.showPassword ? <FontAwesomeIcon icon={faEyeSlash} ></FontAwesomeIcon> : <FontAwesomeIcon onClick={handleClickShowPassword}icon={faEye} className="see-icon"></FontAwesomeIcon>}
                </InputGroup.Text>
              </InputGroup>
              </Form.Group>
              <div className="terms text-center d-flex justify-content-center istok-font my-2">
                <span>By creating an account, you agree to our <span className="fw-bold">terms of use</span> and <span className="fw-bold">privacy policy</span></span>
              </div>
              <div className="d-flex justify-content-center py-3">
                <Button disabled={isDisabled} variant="primary" type='submit' id="register-btn" className="px-5 py-3">REGISTER</Button>
              </div>
            </div>
          </Col>
        </Row>
      </Form>
    </>
  )
}