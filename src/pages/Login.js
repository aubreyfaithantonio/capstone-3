import {useEffect, useState} from "react";
import LoginForm from "../components/LoginForm";
import { Container, Row, Col} from 'react-bootstrap';
import Image from 'react-bootstrap/Image'
import {Link, useNavigate, useParams} from 'react-router-dom'
import {allowedUserLevels} from "../common/Global";

export default function Login() {
  const navigate = useNavigate()
  const {user_level} = useParams()
  
  useEffect(() => {
    if (allowedUserLevels.includes(user_level.toLowerCase()) === false) {
      navigate('/error')
    }
  }, [user_level])
  const pageData = {
    user: {
      header_text: 'USER LOGIN',
      account_switch_text: 'Login as Merchant',
      account_link_switch: '/login/merchant',
      account_register_link: '/register/user',
      redirect: '/shop'
    },
    merchant: {
      header_text: 'MERCHANT LOGIN',
      account_switch_text: 'Login as User',
      account_link_switch: '/login/user',
      account_register_link: '/register/merchant',
      redirect: '/merchant/'
    },
    admin: {
      header_text: 'ADMIN LOGIN',
      account_switch_text: '',
      account_link_switch: '',
      account_register_link: '',
      redirect: '/admin'
    }
  }
  
  let header_text = pageData[user_level].header_text
  let account_switch_text = pageData[user_level].account_switch_text
  let account_link_switch = pageData[user_level].account_link_switch
  let account_register_link = pageData[user_level].account_register_link
  let redirect = pageData[user_level].redirect
  let table_class = 'form-field col-8'
  table_class += user_level === 'admin' ? ' offset-md-2' : ''
  let renderUserImage = () => {
    if (user_level === 'user') {
      return <Col className="col-4 no-padding">
        <div className="user-login-img">
          <Image src="/images/user-login.jpg" fluid></Image>
        </div>
      </Col>
    }
    return ''
  }
  let renderMerchantImage = () => {
    if (user_level === 'merchant') {
      return <Col className="col-4 no-padding">
        <div className="merchant-login-img">
          <Image src="/images/merchant-login.jpg" fluid></Image>
        </div>
      </Col>
    }
    return ''
  }
  let show_footer_note = () => {
    if (user_level !== 'admin') {
      return <div className="mt-3 login-footer istok-font">
        <div className="account-register">
          Don't have an account yet? <Link className="fw-bold" as={Link} to={account_register_link} >Register here</Link>
        </div>
        <div className="account-switch">
          <Link className="fw-bold" as={Link} to={account_link_switch}>{account_switch_text}</Link>
        </div>
      </div>
    }
    table_class += ' offset-md-2'
    return ''
  }
  
  return (
    <>
      <div className="login-card">
        <Container>
          <Row>
            {renderUserImage()}
            <Col className={table_class}>
              <div className="form-header d-flex justify-content-center bebas-font">
                {header_text}
              </div>
              <div className="login-card-internal">
                <LoginForm user_level={user_level} redirect_link={redirect}/>
              </div>
              <br/>
              {show_footer_note()}
            </Col>
            {renderMerchantImage()}
          </Row>
        </Container>
      </div>
    </>
  )
}