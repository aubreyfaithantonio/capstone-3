import {Link} from 'react-router-dom'
import {Button, Card} from "react-bootstrap";

export default function ErrorPage(){
	return (
		<>
		<Card className="text-center">
			<Card.Header><h1>Oops!</h1></Card.Header>
			<Card.Body>
				<Card.Title><p>Page not found</p></Card.Title>
				<Card.Text>
				We looked everywhere for this page. Are you sure the website URL is correct? 
				</Card.Text>
				<Button as={Link} to="/shop" variant="link" className="backToHome">GO BACK TO HOME</Button>
			</Card.Body>
    	</Card>
		</>
	)
}