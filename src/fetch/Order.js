const url = `${process.env.REACT_APP_API_URL}/orders`

const checkout = (cartId) => {
    let access_token = localStorage.getItem('token')
    return fetch(url + '/checkout/' + cartId, {
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + access_token
      }
    })
      .then(response => response.json())
      .then(result => {
        return result
      })
  }

  const allOrders = () => {
    let access_token = localStorage.getItem('token')
    return fetch(url + '/all', {
      method: 'GET',
      headers: {
        Authorization: 'Bearer ' + access_token
      }
    })
      .then(response => response.json())
      .then(result => {
        console.log(result)
        return result
      })
  }

  export { checkout, allOrders }