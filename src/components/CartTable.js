import {Table, Card, Button, Row, Col, Container, Modal, InputGroup, Form} from "react-bootstrap";
import { Link, useNavigate } from 'react-router-dom'
import css from '../css/CartTable.css'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faTrashCan, faPencil, faCircleArrowLeft, faCheck, faXmark} from '@fortawesome/free-solid-svg-icons'
import {useEffect, useState} from "react";
import {cartDetails, removeProduct, updateCart} from "../fetch/Cart";
import {convertTimestamp, basicAlert} from "../common/Global";

export default function OrdersTable(){
	const [count, setCount] = useState(1)
	const [cartIndivDetails, setCartIndivDetails] = useState('')
	const [updateListener, setUpdateListener] = useState(false)
	const [cartData, setCartData] = useState({
		userId:'',
		products: [],
		subtotalAmount: [0]
	});

	// Modals
	const [modalShow, setModalShow] = useState(false);
	const changeQ = (cart) => {
		setCount(cart.quantity)
		setCartIndivDetails(cart)
		setModalShow(true)
	}

	const changeQsave = () => {
		updateCart(cartIndivDetails.productId, count, cartData._id).then((result) => {
			basicAlert('success', 'Success!', result.message).then(() => {
				setUpdateListener(true)
				setModalShow(false)
			})
		})
	}

	const removeItem = (cart) => {
		removeProduct(cart.productId, cartData._id).then((result) => {
			basicAlert('success', 'Success!', result.message).then(() => {
				setUpdateListener(true)
			})
		})
	}
	
	useEffect(() => {
		cartDetails().then(result => {
			setCartData(result)
		})
	},[updateListener])
	
	return(
		<>
			<Container>
				<Card className="cart-card p-3">
					<Row>
						<Col className="my-5">
							<Row className="cart-title-container">
								<Col className="go-back">
									<Button variant="outline-light" as={Link} to="/shop"><FontAwesomeIcon icon={faCircleArrowLeft} className="back-icon"></FontAwesomeIcon></Button>
								</Col>
								<Col className="cart-title">
									<span>CART</span>
								</Col>
								<Col>
									
								</Col>
							</Row>
							<Row>
								<Table striped bordered className="orders text-center" responsive>
									<thead className="table-title">
										<tr className="istok-font table-title-text">
										    <th>PRODUCT ID</th>
										    <th width="20%">NAME </th>
										    <th width="35%">DESCRIPTION</th>
										    <th>PRICE</th>
										    <th>QUANTITY</th>
										    <th>SUBTOTAL</th>
										    <th>ACTIONS</th>
									  	</tr>
									</thead>
									<tbody>
									{cartData.products.map((cart , key) => (
										<tr key={key}>
											<td>{cart._id}</td>
											<td>{cart.productName}</td>
											<td>{cart.productDescription}</td>
											<td>&#8369;{cart.productPrice}</td>
											<td>{cart.quantity}</td>
											<td>{cartData["subtotalAmount"][key]}</td>
											<td width="10%">
												<span><Button onClick={() => changeQ(cart)} variant="outline-secondary" className="edit-button mx-2"><FontAwesomeIcon icon={faPencil} className="editQ-icon"></FontAwesomeIcon></Button></span>
												<span><Button variant="outline-secondary" className="delete-button" onClick={() => removeItem(cart)}><FontAwesomeIcon icon={faTrashCan} className="delete-icon"></FontAwesomeIcon></Button></span>
											</td>
										</tr>
									))}
									{(cartData.products.length === 0) ?
										<tr>
											<td colSpan={7}>Your cart is empty</td>
										</tr>
										:
										''
									}
									</tbody>
								</Table>
							</Row>
							{(cartData.products.length !== 0) ?
								<Row>
									<Col md={{offset:9}} className="checkout-title-container">
										<Button className="go-checkout px-5 py-2" variant="outline-light" as={Link} to={"/checkout/" + cartData._id}>CHECKOUT</Button>
									</Col>
								</Row>
								:
								''
							}
						</Col>
					</Row>
				</Card>
			</Container>

			<Modal size="lg" aria-labelledby="contained-modal-title-vcenter" centered show={modalShow}
       		 onHide={() => setModalShow(false)}>

				<Modal.Header closeButton className="modal-view-title">
					<Modal.Title id="contained-modal-title-vcenter">CHANGE PRODUCT QUANTITY</Modal.Title>
				</Modal.Header>
				<Modal.Body>
				<Container>
						<Row>
							<Col className="wrapper">
								<InputGroup >
									<Button className="plus-btn" onClick={() => setCount(count + 1)} variant="outline-secondary">+</Button>
									<Form.Control disabled type="number" value={count} className="quantity"/>
									<Button className="minus-btn" 
									onClick={ count <= 25 ? () => {
										if(count <= 1){
											setCount(1)
										} else {
											setCount(count-1)
										}
									} : setCount(25) } 
									variant="outline-secondary">-</Button>
								</InputGroup>

								<Button variant="outline-secondary" className="save" onClick={() => changeQsave()}>SAVE <FontAwesomeIcon icon={faCheck}></FontAwesomeIcon></Button> 

								<Button variant="outline-secondary" className="cancel" onClick={() => setModalShow(false)}>CANCEL <FontAwesomeIcon icon={faXmark}></FontAwesomeIcon></Button>
							</Col>
						</Row>
					</Container>
				</Modal.Body>
    		</Modal>
		
		</>
	)
}