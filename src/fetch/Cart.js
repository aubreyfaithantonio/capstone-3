const url = `${process.env.REACT_APP_API_URL}/cart`

const cartDetails = () => {
  let access_token = localStorage.getItem('token')
  return fetch(url + '/get', {
    method: 'GET',
    headers: {
      Authorization: 'Bearer ' + access_token
    }
  })
    .then(response => response.json())
    .then(result => {
      return result
    })
}

const addToCart = (productId, quantity) => {
  let access_token = localStorage.getItem('token')
  return fetch(url + '/add', {
    method: 'POST',
    headers: {
      "Content-Type": "application/json",
      Authorization: 'Bearer ' + access_token
    },
    body: JSON.stringify({
      productId: productId,
      quantity: quantity
    })
  })
    .then(response => response.json())
    .then(result => {
      return result  
    })
}

const updateCart = (productId, quantity, cartId) => {
  let access_token = localStorage.getItem('token')
  return fetch(url + '/update', {
    method: 'PATCH',
    headers: {
      "Content-Type": "application/json",
      Authorization: 'Bearer ' + access_token
    },
    body: JSON.stringify({
      productId: productId,
      quantity: quantity,
      cartId: cartId
    })
  })
  .then(response => response.json())
  .then(result => {
    return result  
  })

}


const removeProduct = (productId, cartId) => {
  let access_token = localStorage.getItem('token')
  return fetch(url + '/removeProduct', {
    method: 'DELETE',
    headers: {
      "Content-Type": "application/json",
      Authorization: 'Bearer ' + access_token
    },
    body: JSON.stringify({
      productId: productId,
      cartId: cartId
    })
  })
  .then(response => response.json())
  .then(result => {
    return result  
  })
}
export { cartDetails, addToCart, updateCart, removeProduct }