import {Button, Form, Modal} from "react-bootstrap";
import {useEffect, useState} from 'react';
import {createOrUpdateProduct} from "../fetch/Product";
import {basicAlert} from "../common/Global";

export default function ProductModal(props) {
  
  const [productName, setProductName] = useState('');
  const [productCategory, setProductCategory] = useState('');
  const [productDesc, setProductDesc] = useState('');
  const [productPrice, setProductPrice] = useState(0);
  const [productStocks, setProductStocks] = useState(0);
  const [modalAction, setModalAction] = useState('')
  const [headerText, setHeaderText] = useState('')
  const [url, setUrl] = useState('');

  function createProduct(event) {
    event.preventDefault()
    let product = {
      category: productCategory === '' ? 'Drinks & Beverages' : productCategory,
      name: productName,
      description: productDesc,
      price: productPrice,
      stocks: productStocks,
      url: url,
      method: props.products.action === 'create' ? 'POST' : 'PATCH'
    }
  
    createOrUpdateProduct(product).then(result => {
      basicAlert('success', 'Success!', result.message).then(() => {
        props.onHide();
        props.modalEvent();
      })
    })
  }
  
  useEffect(() => {
    if (props.products !== null) {
      setProductCategory(props.products.category)
      setProductName(props.products.name)
      setProductDesc(props.products.description)
      setProductPrice(props.products.price)
      setProductStocks(props.products.stocks)
      setHeaderText(props.products.action === 'create' ? 'Create Product' : 'Update Product')
      setUrl(props.products.action === 'create' ? '/create' : '/update/' + props.products.id)
      setModalAction(props.products.modalAction)
    }
  }, [props.products]);
  
  return (
    <>
      <Modal {...props} centered>
        <Modal.Header className="d-flex justify-content-center">
          <Modal.Title>{headerText}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={event => createProduct(event)}>
          <Form.Group className="mb-3">
              <Form.Label>Category:</Form.Label>
              <Form.Control as="select" aria-label="Default select example" value={productCategory} onChange={event => setProductCategory(event.target.value)}>
                <option value="Drinks & Beverages">Drinks & Beverages</option>
                <option value="Desserts & Snacks">Desserts & Snacks</option>
                <option value="Cooking Essentials">Cooking Essentials</option>
                <option value="Health & Beauty">Health & Beauty</option>
                <option value="Household Supplies">Household Supplies</option>
                <option value="Cellphone Load">Cellphone Load</option>
                <option value="Miscellaneous">Miscellaneous</option>
              </Form.Control>
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>Name:</Form.Label>
              <Form.Control type="text" value={productName} onChange={event => setProductName(event.target.value)} />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>Description</Form.Label>
              <Form.Control type="text" value={productDesc} onChange={event => setProductDesc(event.target.value)} />
            </Form.Group>
  
            <Form.Group className="mb-3" >
              <Form.Label>Price</Form.Label>
              <Form.Control type="number" value={productPrice} onChange={event => setProductPrice(event.target.value)}/>
            </Form.Group>
  
            <Form.Group className="mb-3" >
              <Form.Label>Stocks</Form.Label>
              <Form.Control type="number" value={productStocks} onChange={event => setProductStocks(event.target.value)}/>
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={props.onHide}>
            Close
          </Button>
          <Button type="submit" variant="primary" onClick={createProduct}>
            {modalAction}
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  )
}