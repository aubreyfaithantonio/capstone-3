import {Navbar, Nav, NavDropdown, Container, Row, Col, Badge} from 'react-bootstrap'
import { Link, NavLink } from 'react-router-dom'
import '../css/AppNavbar.css'
import UserContext from "../UserContext";
import {useContext} from "react";

export default function AppNavbar(){
	const {user} = useContext(UserContext)
	
	const dropdown = () => {
		if (user.level === 'user') {
			return <>
				<NavDropdown.Item as={Link} to={'profile/' + user.id}>MY ACCOUNT</NavDropdown.Item>
				<NavDropdown.Item as={Link} to="/logout">LOGOUT</NavDropdown.Item>
			</>
		} else if (user.level === 'merchant') {
			return <>
				<NavDropdown.Item as={Link} to={'merchant/' + user.id}>DASHBOARD</NavDropdown.Item>
				<NavDropdown.Item as={Link} to="/logout">LOGOUT</NavDropdown.Item>
			</>
		} else if(user.level === 'admin') {
			return <NavDropdown.Item as={Link} to="/logout">LOGOUT</NavDropdown.Item>
		} else {
			return <>
				<NavDropdown.Item as={Link} to="/login/user">LOGIN</NavDropdown.Item>
				<NavDropdown.Item as={Link} to="/register/user">REGISTER</NavDropdown.Item>
			</>
		}
	}
	
	return(
      	<Container >
	      	<Navbar fixed="top" className="d-flex justify-content-around">
		      	<Row>
		      		<Col><Nav.Link as={Link} to="/" className="appnavbar-link">HOME</Nav.Link></Col>
		      		<Col><Nav.Link as={Link} to="/about" className="appnavbar-link" href="#link">ABOUT</Nav.Link></Col>
		      		<Col><Nav.Link as={Link} to="/merchant" className="appnavbar-link" href="#link">MERCHANT</Nav.Link></Col>
		      	</Row>
		      	<Row>
		      		<Col>
			      		<Navbar.Brand as={Link} to="/">
			      		     <img
			      		      src="/images/NavbarLogo.png"
			      		      width="250"
			      		      height="76"
			      		      className="img-fluid d-inline-block align-top"
			      		      alt=""
			      		    />
			      		</Navbar.Brand>
		      		</Col>
		      	</Row>
		      	<Row>
							{user.level === 'user' || user.level === null ?
							<>
								<Col><Nav.Link as={Link} to="/shop" className="appnavbar-link" href="#link">SHOP</Nav.Link></Col>
								<Col><Nav.Link as={Link} to="/cart" className="appnavbar-link" href="#link">CART</Nav.Link></Col>
							</> :
								''
							}
		      		<Col>
		      			<NavDropdown className="appnavbar-link" title="ACCOUNT" id="appnavbarScrollingDropdown">
									{dropdown()}
		      			</NavDropdown>
		      		</Col>  
		      	</Row>
		   	</Navbar>
      	</Container>
	)
}