import {Nav, Container, Row, Col, Card, Button} from 'react-bootstrap'
import '../css/MerchantPage.css'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faChartLine, faPesoSign, faBan, faIdCard} from '@fortawesome/free-solid-svg-icons'
import { Link, NavLink } from 'react-router-dom'


export default function MerchantPage(){
	
	const myStyle={
		height: '87.5vh',
        backgroundImage: "url('/images/merchant-page-bg.jpg')",
        backgroundPosition: 'center',
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        display: 'flex',
        justifyContent: 'center'
    };

	return(
		<>
		<div style={myStyle} className="merchant-page-bg">
			<Container fluid className="mt-5 merchant-page-case">
				<Row>
					<Col className="col-6 left-side">
						<Row>
							<Card className="featured-m p-3">
							      <Card.Body>
							        <Card.Title className="featured-m-title">MERCHANT FEEDBACK</Card.Title>
							        <br></br>
							        <Card.Subtitle className="featured-m-name"><u>Milagros Santiago, 57</u></Card.Subtitle>
							        <br></br>
							        <Card.Text className="featured-m-text">
							        <p>
							        	"Sa tulong ng website na ito, mas lalong lumago ang aking negosyo. Tinayo ko ang tindahan ko parang makatulong sa aming pang araw-araw na pangangailangan at nakatulong din ito upang mapagaral ko ang aking mga anak."
							        </p>
							        </Card.Text>
							      </Card.Body>
							    </Card>
						</Row>
						<Row>
							<Card className="featured-m p-3">
							    <Card.Body>
							       	<Card.Text className="featured-m-quote">
							        <p>
							            <em>You don't need prior knowledge to successfully market your business online. We are here to help you increase your profit and reach a wider range of customers.</em>
							        </p>
							        </Card.Text>
							    </Card.Body>
							</Card>
						</Row>
					</Col>

					<Col className="right-side-nav">
						<Nav className="right-side p-5 justify-content-center align-item-center">
							<Row className="flex-column reasons-case">
								<Row className="flex-column">
									<Col><h3 className="reasons-title">BE ONE OF OUR MERCHANTS</h3></Col>    
								</Row>
								<Row className="reasons flex-column">
									<Col className="mt-3"><Nav.Item className="be-one"><FontAwesomeIcon icon={faPesoSign} className="icon"></FontAwesomeIcon> Sell right away</Nav.Item></Col>
									<Col className="mt-3"><Nav.Item className="be-one"><FontAwesomeIcon icon={faChartLine} className="icon"></FontAwesomeIcon> Grow your market</Nav.Item></Col>
									<Col className="mt-3"><Nav.Item className="be-one"><FontAwesomeIcon icon={faBan} className="icon"></FontAwesomeIcon> Zero Hidden Fees</Nav.Item></Col>
									<Col className="mt-3"><Nav.Item className="be-one"><FontAwesomeIcon icon={faIdCard} className="icon"></FontAwesomeIcon> Free Registration</Nav.Item></Col>
									<Button as={Link} to="/register/merchant" variant="outline-light" className="register-merchant mt-4 py-2">REGISTER</Button>
								</Row>
							</Row>
						</Nav>
					</Col>
				</Row>
			</Container>
		</div>
		<div className="overlay"></div>
		</>
	)
}